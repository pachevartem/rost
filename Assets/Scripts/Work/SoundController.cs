﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{


	public  AudioSource _AudioSource;
	
	// Use this for initialization
	private void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}


	public bool MuteAudio()
	{
		_AudioSource.mute = !_AudioSource.mute;
		return _AudioSource.mute;
	}


}
