﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;


namespace ArtelRostech
{
	[RequireComponent(typeof(EventTrigger))]
	public class ArtelButton : MonoBehaviour
	{
		/// <summary>
		/// Заполнитель
		/// </summary>
		public Image Image;

		/// <summary>
		/// Действие которое необходимо выполнить
		/// </summary>
		public UnityEvent Method;

		/// <summary>
		/// Метод срабатывающий при наведини курсора
		/// </summary>
		public void ABegin()
		{
			StartCoroutine(CircleQ());
		}

		/// <summary>
		/// Метод срабатывающий при вывода курсора с изображения
		///  </summary>
		public void AStop()
		{
			StopAllCoroutines();
			ResetFill();
		}

		public delegate void PressButton();

		public static PressButton press  = () => { };
		
		IEnumerator CircleQ()
		{
			while (true)
			{
				Image.fillAmount += Time.deltaTime * 0.1f * 5;
				yield return null;
				if (Image.fillAmount > 0.99f)
				{
					ResetFill();
					Method.Invoke();
					press();
					StopAllCoroutines();
				}
			}
		}

		/// <summary>
		/// Сброс заполнилеля
		/// </summary>
		void ResetFill()
		{
			Image.fillAmount = 0;
		}

	}
}