﻿using System;
using System.Collections;
using UnityEngine;

namespace ArtelRostech
{
    public class DistanceController: MonoBehaviour
    {
        public Transform Head;
        public Transform Display;
       
        private float Distance = 0;
        private Quaternion old_alpha, new_alpha;
        private float Tolerance = 0;
        private GameObject _EnemyObj;

        private void Update()
        {
            SetRotationDisplay();
        }

        void SetRotationDisplay()
        {
            if (ChekDistance())
            {
                return;
            }
            
            old_alpha = Quaternion.Euler(0, Display.transform.localRotation.eulerAngles.y, 0); //  Danya Path
            new_alpha = Quaternion.Euler(0, Head.transform.localRotation.eulerAngles.y, 0);
            Display.transform.localRotation = Quaternion.Lerp(old_alpha, new_alpha, Time.deltaTime*3);
//            
//            Display.rotation =  Quaternion.Slerp(Display.rotation, Head.rotation, Time.deltaTime*1.2f);  // PachevPath 
//            Display.eulerAngles = new Vector3(0,Display.eulerAngles.y,0);
        }

        
        bool ChekDistance()
        {
           return Math.Abs(Display.eulerAngles.y - Head.eulerAngles.y) < 40;
        }


    }
}