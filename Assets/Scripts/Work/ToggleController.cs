﻿using UnityEngine;
using UnityEngine.UI;

namespace ArtelRostech
{
    public class ToggleController: MonoBehaviour //TODO: Надо разобраться с визуальным решением Toggle.Select() 
    {
        
        public Toggle Toggle;

        public void SetTog()
        {
            Toggle.isOn = !Toggle.isOn;
        }
    }
}