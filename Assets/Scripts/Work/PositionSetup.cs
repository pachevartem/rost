﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionSetup : MonoBehaviour
{

    public Material LMat;
    public Material RMat;
    public Texture _mainTexture;
    
    private void Awake()
    {
        SetupTexture();
    }

    void SetupTexture()
    {
        LMat.mainTexture = _mainTexture;
        RMat.mainTexture = _mainTexture;
    }
}
