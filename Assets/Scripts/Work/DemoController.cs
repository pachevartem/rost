﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoController : MonoBehaviour
{

	public List<PositionSetup> PositionSetups;
	public GameObject Head;
	private int _position;

	public static DemoController Instance;
	
	private void Awake()
	{
		Instance = this;
	}

	public int Position
	{
		get { return _position; }
		set
		{
			if (value==PositionSetups.Count || value<0)
			{
				Position = 0;
				return;
			}
			_position = value;
			Head.transform.position = PositionSetups[value].gameObject.transform.position;
		}
	}

	public void NextScence()
	{
		Position++;
	}
	
	public void PrewScence()
	{
		Position--;
	}


	private void Update()
	{
		if (Input.anyKeyDown)
		{
//			NextScence();
		}
	}
}
