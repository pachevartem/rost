﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArtelRostech
{
    public class ManagerSound : MonoBehaviour
    {

        /// <summary>
        /// Нажатие на кнопку
        /// </summary>
        public AudioClip ClickSound;

        /// <summary>
        /// Фоновая музыка
        /// </summary>
        public AudioClip AmbientSound;

        public AudioSource _AudioSource;
        
        
        public static ManagerSound instance;

        void Singleton()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);   
            }
            DontDestroyOnLoad(gameObject);
        }

     
        
        private bool sound;
        public bool Sound
        {
            get { return sound; }
            set { sound = value; }
        }

        void Awake()
        {
            Singleton();
            ArtelButton.press += PlaySound;
            ButtonSoundToggle.onOff += ToggleSoundApplication;
            _AudioSource.mute = true;
            _AudioSource.Play();
        }

        private void PlaySound()
        {
            if (Sound)
            {
                AudioSource.PlayClipAtPoint(ClickSound, Vector3.zero);
            }
        }

        public void SetSound()
        {
            Sound = true;
            
            _AudioSource.mute = false;
//            _AudioSource.PlayOneShot(_AudioSource.clip);
           
        }
        
        public void OffSound()
        {
            Sound = false;
        }


        public void StartBackground()
        {
            AudioSource.PlayClipAtPoint(AmbientSound, Vector3.zero);
        }


        private int k = 0;

        void ToggleSoundApplication()
        {
            if (k%2==0)
            {
                _AudioSource.mute = true;
            }
            else
            {
                _AudioSource.mute = false;
            }
            k++;
        }

        


    }
}