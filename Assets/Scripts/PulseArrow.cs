﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseArrow : MonoBehaviour
{
	
	private Vector3 _start;
	public bool _run = true;

	public int Scaler = 5;
		
	void Awake()
	{
		_start = transform.localScale;
		StartCoroutine(Pulse());
	}



	IEnumerator Pulse()
	{
		int count = 0;
		
		while (true)
		{
			if (_run)
			{
				if (count%10==0)
				{
					Scaler = -Scaler;
				}
				transform.localScale += Vector3.one * Time.deltaTime * Scaler;
				count++;
				yield return null;
				
			}
			yield return null;
		}
	}
}
