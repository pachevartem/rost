﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateMaterials : MonoBehaviour
{

	public List<Texture> AnimMaterials;

	public Material OwnMaterial;

	public bool OneShot = false;
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(StartAnimation());
	}

	IEnumerator StartAnimation()
	{

		while (true)
		{
			for (int i = 0; i < AnimMaterials.Count; i++)
			{
				OwnMaterial.mainTexture = AnimMaterials[i];
				yield return new WaitForSeconds(Time.deltaTime);
			}
			if (OneShot)
			{
				yield break;
			}
		}
		yield return null;
	}
}
