﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSoundToggle : MonoBehaviour {

    public delegate void ToggleSound();
       
    public static ToggleSound onOff = () => { };

    public void ToggleSoundUI()
    {
        onOff();
    }
}
