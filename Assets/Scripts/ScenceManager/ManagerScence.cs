﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace RosTechSity
{
    
    public enum PanoName
    {
        TUTORIAL,EXPO,FOUNTAN,HALL,PARKING,ROOF,SHWABE,SPA,STOLOVKA,TOWER,VIPROOM
    }

    public  class ManagerScence: MonoBehaviour
    {
        
        public static ManagerScence instance;

        
        
        
        void Singleton()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);   
            }
            DontDestroyOnLoad(gameObject);
        }

        private void Awake()
        {
//            Singleton();
        }

        public void GoToScence(string name)
        {
          SceneManager.LoadScene(name);
        }
    }
}