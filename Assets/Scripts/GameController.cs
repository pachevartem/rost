﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RosTechSity
{



    public class GameController : MonoBehaviour
    {
        /// <summary>
        /// Ссылки на все панорамы
        /// </summary>
        public List<Texture> Panorams;

        /// <summary>
        /// Ссылка на материал для левого глаза
        /// </summary>
        public Material LPanoramSphere;
    
        /// <summary>
        /// Ссылка на материал для правого глаза
        /// </summary>
        public Material RPanoramSphere;
        
        /// <summary>
        /// Перечесление локаций
        /// </summary>
        public PanoName _panoName = PanoName.TOWER;

        private void Start()
        {
            SetLoaction(_panoName);
        }

        public void SetLoaction(PanoName panoName)
        {
            switch (panoName)
            {
                case PanoName.EXPO:
                    SetTexture(Panorams[0]);
                    break;
                case PanoName.FOUNTAN:
                    SetTexture(Panorams[1]);
                    break;
                case PanoName.HALL:
                    SetTexture(Panorams[2]);
                    break;
                case PanoName.PARKING:
                    SetTexture(Panorams[3]);
                    break;
                case PanoName.ROOF:
                    SetTexture(Panorams[4]);
                    break;
                case PanoName.SHWABE:
                    SetTexture(Panorams[5]);
                    break;
                case PanoName.SPA:
                    SetTexture(Panorams[6]);
                    break;
                case PanoName.STOLOVKA:
                    SetTexture(Panorams[7]);
                    break;
                case PanoName.TOWER:
                    SetTexture(Panorams[8]);
                    break;
                case PanoName.VIPROOM:
                    SetTexture(Panorams[9]);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("panoName", panoName, null);
            }
        }

        private void SetTexture(Texture texture)
        {
            LPanoramSphere.mainTexture = texture;
            RPanoramSphere.mainTexture = texture;
        }

    }
}
