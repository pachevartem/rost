﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayManager : MonoBehaviour
{

    public GameObject SelectedCamera;
    [Range(2,8)]
    public float DistanceCamera = 4;

    void Start()
    {
        SetDestanceDisplay(DistanceCamera);
    }

    private void Update()
    {
//        transform.LookAt(SelectedCamera.transform);
    }

    void SetDestanceDisplay(float distance)
    {
        transform.position = SelectedCamera.transform.forward.normalized*DistanceCamera;
    }


}
