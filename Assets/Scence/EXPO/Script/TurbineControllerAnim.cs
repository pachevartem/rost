﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurbineControllerAnim : MonoBehaviour {

	
	public List<Texture> TexturesAnim;
	public Material AnimMat_Left;
	public Material AnimMat_Right;


	public void StartAnim()
	{
		StartCoroutine(StartAnimation());
	}

	public void EndAnim()
	{
		StartCoroutine(EndAnimation());
	}

	private int k = 0;
	public void ToggleAnim()
	{
		if (k%2==0)
		{
			StartAnim();
		}
		else
		{
			EndAnim();
		}
		k++;
	}


	IEnumerator StartAnimation()
	{

		while (true)
		{
			for (int i = 0; i < TexturesAnim.Count; i++)
			{
				AnimMat_Left.mainTexture = TexturesAnim[i];
				AnimMat_Right.mainTexture = TexturesAnim[i];
				yield return new WaitForSeconds(Time.deltaTime);
			}
			yield break;
		}
		yield return null;
	}
	
	IEnumerator EndAnimation()
	{

		while (true)
		{
			for (int i = TexturesAnim.Count-1; i > 0; i--)
			{
				AnimMat_Left.mainTexture = TexturesAnim[i];
				AnimMat_Right.mainTexture = TexturesAnim[i];
				yield return new WaitForSeconds(Time.deltaTime);
			}
			yield break;
		}
		yield return null;
	}
}
