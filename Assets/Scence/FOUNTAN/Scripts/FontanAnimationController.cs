﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FontanAnimationController : MonoBehaviour
{
	//
	public List<Texture> FountanTextures;
	public Material FLeft;
	public Material FRight;
	//
	public List<Texture> FlagTextures;
	public Material FlagLeft;
	public Material FlagRight;
	//


	private void Start()
	{
		StartCoroutine(LoopAnimation(FlagLeft, FlagRight, FlagTextures));
		StartCoroutine(LoopAnimation(FLeft, FRight, FountanTextures));
	}


	IEnumerator LoopAnimation(Material matLeft, Material matRight, List<Texture> textures)
	{
		while (true) 
		{
			for (int i = 0; i < textures.Count; i++)
			{
				matLeft.mainTexture = textures[i];
				matRight.mainTexture = textures[i];
				yield return new WaitForSeconds(Time.deltaTime);
			}
		}
	}
	

	
}
